
import numpy as np
import pandas as pd
import json, collections


with open('dico.json', encoding='utf-8') as fichier_json:
    # Charger le contenu JSON existant dans un dictionnaire
    d = json.load(fichier_json)

# Ca c'est une fonction qui permet de parcourir les trente mille tiroir du json pour extraire le genre de chaque intervenant, pour chaque année, pour chaque séance. 
# Le nom complet du GdT est à passer en argument, il faudra itérer la fonction sur chaque nom de groupe pour sortir un tableau par groupe.
    
def gender_interv(nomgroupe):
    e = d[nomgroupe]["année"]
    l = []
    for f in e.keys():
        l.append(f)
        gender = []
        for value in e[f]["séances"].values():
            for value in value["interv"].values():
                gender.append(value["gender"])
        l.append(gender)

    data = {}
    current_year = None

# Ensuite je transforme la liste en dico en faisant une collection (je comptabilise le total de nombre de M, F) par année. 
# Exemple : {'2021': {'M': 6, 'F': 3}, '2022': {'M': 2, 'F': 1}, '2023': {'F': 2, 'M': 1}}. Ca se transforme en stackplot facile apparemment, ça m'a surpris !
     
    for item in l:
        if type(item) is str:
            current_year = item
        elif type(item) is list:
            data[current_year] = dict(collections.Counter(item))

    return data

