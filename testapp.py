import streamlit as st
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from viewgen import gender_interv

# Pour transformer le dico en dataframe (transposé) puis en stackplot. (la suite au prochain épisode : streamlit.py)

df = pd.DataFrame(gender_interv("Santé, numérique et intelligence artificielle")).T

st.title('GDR2091 Gender Monitor')

st.bar_chart(df, color=["#ffb233","#73d686"])
# # Plotting the histogram
# df.plot(kind='bar', stacked=True, color=['blue', 'pink'])

# # Adding labels and title
# plt.xlabel('Year')
# plt.ylabel('Count')
# plt.title('Gender Distribution Over Years')

# # Display the plot
# plt.show()