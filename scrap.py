import requests
import json
from bs4 import BeautifulSoup
URL = "https://cis.cnrs.fr/groupes-de-travail-gdr/"

page = requests.get(URL)
soup = BeautifulSoup(page.content, "html.parser")
titles = soup.find_all("h3")


# # creation de la structure de dictionnaire. 

# #je scrappe les titre h3 de la page GDR et je les mets dans une liste
# liGroupes = [title.a for title in titles if title.a is not None]

# d = {}

# for e in liGroupes:
#     d[e.text] = {"lien":e.get('href'), "année":{k:{"coordo":{l:{"name":"","surname":"","gender":""}for l in range(1,4)},"séances":{j:{"fullText":"","GPTKeywords":[],"interv":{m:{"name":"","surname":"","gender":""}for m in range(1,4)}}for j in range(1,10)}} for k in range(2019,2025)}}

# with open('dico.json', 'w', encoding='utf-8') as fichier_json:
#     # Charger le contenu JSON existant dans un dictionnaire
#     json.dump(d, fichier_json, indent=4)